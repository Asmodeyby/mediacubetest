<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartmentWorkerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('department_worker', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('department_id')->unsigned();
            $table->unsignedInteger('worker_id')->unsigned();
        });

        Schema::table('department_worker', function (Blueprint $table) {
            $table->foreign('worker_id')->references('id')->on('workers');
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('department_worker');
    }
}
