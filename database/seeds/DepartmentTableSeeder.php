<?php

use Illuminate\Database\Seeder;
use App\Department;

class DepartmentTableSeeder extends Seeder
{

    private $department_count = 5;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Department::truncate();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < $this->department_count; $i++) {

            Department::create(
                [
                    "title" => $faker->unique()->company
                ]
            );

        }
    }
}
