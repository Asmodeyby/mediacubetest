<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Worker;

class DepartmentWorkerTableSeeder extends Seeder
{
    private $max_departments_per_worker = 2;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $workers = Worker::all();
        $departments = Department::all();

        DB::table("department_worker")->truncate();

        foreach($workers as $worker) {
            $worker->departments()->attach($departments->random(rand(1, $this->max_departments_per_worker))->pluck("id")->toArray());
        }

        foreach($departments as $department) {
            $department->calculateWorkerCount();
            $department->calculateMaxWorkerSalary();
        }
    }
}
