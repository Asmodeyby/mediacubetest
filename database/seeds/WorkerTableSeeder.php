<?php

use Illuminate\Database\Seeder;
use App\Worker;

class WorkerTableSeeder extends Seeder
{

    private $worker_count = 20;
    private $min_worker_salary = 500;
    private $max_worker_salary = 2500;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Worker::truncate();

        $faker = Faker\Factory::create();

        for ($i = 0; $i < $this->worker_count; $i++) {

            $sex = $faker->randomElement(["male", "female"]);

            Worker::create(
                [
                    "first_name" => $faker->unique()->firstName($sex),
                    "last_name" => $faker->unique()->lastName,
                    "middle_name" => $faker->unique()->colorName,
                    "salary" => $faker->randomFloat(0, $this->min_worker_salary, $this->max_worker_salary),
                    "sex" => $sex
                ]
            );
        }
    }
}
