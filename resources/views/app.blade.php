@php
    $config = [
        'domain'          => config('app.url'),
        'appName'         => config('app.name'),
        'locale'          => $locale = app()->getLocale(),
        'locales'         => config('app.locales'),
        'discordAuth'     => config('services.discord.client_id'),
        'steamAuth'       => true,
        'twitchAuth'      => config('services.twitch.client_id'),
        'twitterAuth'     => config('services.twitter.client_id'),
        'battlenetAuth'   => config('services.battlenet.client_id'),
        'vkontakteAuth'   => config('services.vkontakte.client_id'),
        'facebookAuth'    => config('services.facebook.client_id'),
        'renderDelay'     => 50,
    ];

    $polyfills = [
        'Promise',
        'Object.assign',
        'Object.values',
        'Array.prototype.find',
        'Array.prototype.findIndex',
        'Array.prototype.includes',
        'String.prototype.includes',
        'String.prototype.startsWith',
        'String.prototype.endsWith',
    ];
@endphp
        <!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
</head>
<body class="px-navbar-fixed">

<div id="app"></div>
{{-- Global configuration object --}}
<script>window.config = @json($config);</script>


{{-- Polyfill JS features via polyfill.io --}}
<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features={{ implode(',', $polyfills) }}"></script>

{{-- Load the application scripts --}}
@if (app()->isLocal())
    <script src="{{ mix('js/app.js') }}"></script>
@else
    <script src="{{ mix('js/manifest.js') }}"></script>
    <script src="{{ mix('js/vendor.js') }}"></script>
    <script src="{{ mix('js/app.js') }}"></script>
@endif


</body>
</html>
