const Welcome = () => import('~/pages/welcome').then(m => m.default || m);
const Workers = () => import('~/pages/workers').then(m => m.default || m);
const WorkersDelete = () => import('~/pages/workers_delete').then(m => m.default || m);
const WorkersEdit = () => import('~/pages/workers_edit').then(m => m.default || m);
const Departments = () => import('~/pages/departments').then(m => m.default || m);
const DepartmentsDelete = () => import('~/pages/departments_delete').then(m => m.default || m);
const DepartmentsEdit = () => import('~/pages/departments_edit').then(m => m.default || m);


export default [
    { path: '', redirect: { name: 'home' } },
    { path: '/home', name: 'home', component: Welcome },
    { path: '/workers', name: 'workers', component: Workers},
    { path: '/workers/:id/delete', name: 'workers.delete',  component: WorkersDelete, props: true },
    { path: '/workers/:id/edit', name: 'workers.edit',  component: WorkersEdit, props: true },
    { path: '/workers/create', name: 'workers.create',  component: WorkersEdit, props: true },
    { path: '/departments', name: 'departments', component: Departments },
    { path: '/departments/:id/delete', name: 'departments.delete',  component: DepartmentsDelete, props: true },
    { path: '/departments/:id/edit', name: 'departments.edit',  component: DepartmentsEdit, props: true },
    { path: '/departments/create', name: 'departments.create',  component: DepartmentsEdit, props: true }
]
