import axios from 'axios'
import * as types from '../mutation-types'
import {mapGetters} from 'vuex'
import store from '~/store'

// state
export const state = {
    newsList: [],
    newsListLast: null,
}

// getters
export const getters = {
    newsList: state => state.newsList,
    newsListLast: state => state.newsListLast
}

// mutations
export const mutations = {
    [types.FETCH_NEWS_FAILURE](state) {

    },

    [types.FETCH_NEWS_SUCCESS](state, {response}) {
        state.newsListLast = response;
    },

    [types.ADD_NEWS_IN_LIST](state, {item}) {
        state.newsList.push(item)
    },

    [types.RESET_NEWS_LIST](state) {
        state.newsList = [];
        state.newsListLast = null;
    },
}

// actions
export const actions = {
    async fetchNewsList({commit}, params) {

        try {
            const {data} = await axios.get('/api/news/list', {params: params})
            data.items.map(function(item) {
                commit(types.ADD_NEWS_IN_LIST, {item: item})
            });
            commit(types.FETCH_NEWS_SUCCESS, {response: data})
        } catch (e) {
            console.error(types.FETCH_NEWS_FAILURE, e);
            commit(types.FETCH_NEWS_FAILURE)
        }
    },
    resetNewsList({commit}) {
        commit(types.RESET_NEWS_LIST)
    },
}
