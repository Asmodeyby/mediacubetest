
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

require('./bootstrap');

import Vue from 'vue'
import VueRouter from 'vue-router'
import VueBootstrapToasts from "vue-bootstrap-toasts";
import App from '~/components/App'
import store from '~/store'
import router from '~/router'

import '~/plugins'
import '~/components'

Vue.use(VueRouter);
Vue.use(VueBootstrapToasts);

window.Vue = new Vue({
    store,
    router,
    ...App
})