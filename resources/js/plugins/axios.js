import axios from 'axios'
import store from '~/store'
import router from '~/router'

// Request interceptor
axios.interceptors.request.use(request => {
  return request
})

// Response interceptor
axios.interceptors.response.use(response =>
{
    return response;
}, error => {
  const { status } = error.response
  console.log(error);

    if (status >= 500) {
        if (error.response.data.message != undefined) {
            var error_text = error.response.data.message;
            window.Vue.$toast.error(error_text);
        }
    }

    // Обработка ошибки формы
    if (status === 422) {
        if (error.response.data.message != undefined) {
            var error_text = error.response.data.message;
            window.Vue.$toast.error(error_text);
        }
    }
  return Promise.reject(error)
})
