<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DepartmentTest extends TestCase
{

    private $slug = "/api/department";


    public function setUP() : void {
        parent::setUp();
        $this->seed();
    }

    public function testIndex()
    {
        $response = $this->get("{$this->slug}");
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $response = $this->get("{$this->slug}/1");
        $response->assertStatus(200);
    }

    public function testStore()
    {
        /* CASE #1 */
        $input = [
            "title"     => "title",
        ];

        $response = $this->json("POST","{$this->slug}", $input);
        $response->assertStatus(201);

        $data = json_decode($response->content(), true);
        $this->assertEquals($input['title'], $data['data']['title']);

        /* CASE #2 */
        $response = $this->json("POST","{$this->slug}",
            [
                "title" => "",
            ]
        );
        $response->assertStatus(422);
    }

    public function testDestroy()
    {
        $response = $this->delete("{$this->slug}/5");
        $response->assertStatus(500);

        // Принудительно удаляем отдел, удалив всех его сотрудников
        $response = $this->delete("{$this->slug}/5?force=1");
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        /* CASE #1 */
        $input = [
            "title"     => "title",
        ];

        $response = $this->json("PUT","{$this->slug}/1",  $input);
        $response->assertStatus(200);

        $data = json_decode($response->content(), true);
        $this->assertEquals($input['title'], $data['data']['title']);

        /* CASE #2 */
        $response = $this->json("PUT","{$this->slug}/1",
            [
                "first_name"    => "",
            ]
        );
        $response->assertStatus(422);
    }
}
