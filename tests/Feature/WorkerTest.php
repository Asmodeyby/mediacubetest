<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class WorkerTest extends TestCase
{

    private $slug = "/api/worker";


    public function setUP() : void {
        parent::setUp();
        $this->seed();
    }

    public function testIndex()
    {
        $response = $this->get("{$this->slug}");
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $response = $this->get("{$this->slug}/1");
        $response->assertStatus(200);
    }

    public function testStore()
    {
        /* CASE #1 */
        $input = [
            "first_name"    => "first_name",
            "last_name"     => "last_name",
            "middle_name"   => "middle_name",
            "sex"           => "female",
            "salary"        => 500,
            "departments"   => [1,2,3,4,5]
        ];

        $response = $this->json("POST","{$this->slug}", $input);
        $response->assertStatus(201);

        $data = json_decode($response->content(), true);
        $this->assertEquals($input['first_name'], $data['data']['first_name']);
        $this->assertEquals($input['last_name'], $data['data']['last_name']);
        $this->assertEquals($input['middle_name'], $data['data']['middle_name']);
        $this->assertEquals($input['sex'], $data['data']['sex']);
        $this->assertEquals($input['salary'], $data['data']['salary']);

        /* CASE #2 */
        $input = [
            "first_name"    => "first_name",
            "last_name"     => "last_name",
            "middle_name"   => "middle_name",
            "sex"           => "female",
            "salary"        => 500,
        ];

        $response = $this->json("POST","{$this->slug}", $input);
        $response->assertStatus(422);

        /* CASE #3 */
        $response = $this->json("POST","{$this->slug}",
            [
                "first_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("POST","{$this->slug}",
            [
                "last_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("POST","{$this->slug}",
            [
                "middle_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("POST","{$this->slug}",
            [
                "sex"    => "",
            ]
        );
        $response->assertStatus(422);

        /* CASE #4 */
        $response = $this->json("POST","{$this->slug}",
            [
                "salary"    => -500,
            ]
        );
        $response->assertStatus(422);

        /* CASE #5 */
        $response = $this->json("POST","{$this->slug}",
            [
                "sex"    => "alien",
            ]
        );
        $response->assertStatus(422);
    }

    public function testDestroy()
    {
        $response = $this->delete("{$this->slug}/5");
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        /* CASE #1 */
        $input = [
            "first_name"    => "first_name",
            "last_name"     => "last_name",
            "middle_name"   => "middle_name",
            "sex"           => "female",
            "salary"        => 500,
            "departments"   => [1,2,3,4,5]
        ];

        $response = $this->json("PUT","{$this->slug}/1",  $input);
        $response->assertStatus(200);

        $data = json_decode($response->content(), true);
        $this->assertEquals($input['first_name'], $data['data']['first_name']);
        $this->assertEquals($input['last_name'], $data['data']['last_name']);
        $this->assertEquals($input['middle_name'], $data['data']['middle_name']);
        $this->assertEquals($input['sex'], $data['data']['sex']);
        $this->assertEquals($input['salary'], $data['data']['salary']);

        /* CASE #2 */
        $response = $this->json("PUT","{$this->slug}/1",
            [
                "first_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("PUT","{$this->slug}/1",
            [
                "last_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("PUT","{$this->slug}/1",
            [
                "middle_name"    => "",
            ]
        );
        $response->assertStatus(422);

        $response = $this->json("PUT","{$this->slug}/1",
            [
                "sex"    => "",
            ]
        );
        $response->assertStatus(422);

        /* CASE #3 */
        $response = $this->json("PUT","{$this->slug}/1",
            [
                "salary"    => -500,
            ]
        );
        $response->assertStatus(422);

        /* CASE #4 */
        $response = $this->json("PUT","{$this->slug}/1",
            [
                "sex"    => "alien",
            ]
        );
        $response->assertStatus(422);
    }
}
