<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Department
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $title
 * @property int $worker_count
 * @property int $max_worker_salary
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereMaxWorkerSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Department whereWorkerCount($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Worker[] $workers
 */
class Department extends Model
{
    //

    protected $fillable = ['title'];

    public function workers() {
        return $this->belongsToMany(Worker::class, 'department_worker', "department_id", "worker_id");

    }

    /**
     * @return $this
     */
    public function calculateWorkerCount() {
        $this->worker_count = $this->workers->count();
        $this->save();
        return $this;
    }

    /**
     * @return $this
     */
    public function calculateMaxWorkerSalary() {
        if ($this->workers->count())
            $this->max_worker_salary = $this->workers()->max('salary');
        else
            $this->max_worker_salary = 0;
        $this->save();
        return $this;
    }
}
