<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Worker as WorkerResource;

class Department extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'worker_count' => $this->worker_count,
            'max_worker_salary' => $this->max_worker_salary,
            'workers' => WorkerResource::collection($this->whenLoaded('workers')),
        ];
    }
}
