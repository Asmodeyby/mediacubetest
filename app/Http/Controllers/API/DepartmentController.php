<?php

namespace App\Http\Controllers\API;

use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Department as DepartmentResource;
use Illuminate\Validation\Rule;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $with = $request->query->get('with', []);
        if (count($with))
            $department = Department::with($with)->get();
        else
            $department = Department::all();
        return DepartmentResource::collection($department);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'title' => 'required|string|max:50|unique:departments',
        ]);

        $department = new Department();
        $department->fill($request->all());

        if ($department->save())
            return new DepartmentResource($department);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
        return new DepartmentResource($department);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        //
        $this->validate($request,[
            'title' => ['required', 'string', 'max:50', Rule::unique('departments')->ignore($department)],
        ]);

        $department->fill($request->all());

        if ($department->save())
            return new DepartmentResource($department);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Department $department)
    {
        //
        $force = $request->query->get("force", false);
        if ($force)
            $department->workers()->sync([]);

        if ($department->workers->count())
            abort(500, "Невозможно удалить отдел, в котором есть сотрудники");

        if ($department->delete())
            return new DepartmentResource($department);
    }
}
