<?php

namespace App\Http\Controllers\API;

use App\Department;
use App\Worker;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Worker as WorkerResource;
use Illuminate\Validation\Rule;

class WorkerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $with = $request->query->get('with', []);
        if (count($with))
            $worker = Worker::with($with)->get();
        else
            $worker = Worker::all();
        return WorkerResource::collection($worker);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request,[
            'first_name' => 'string|max:20',
            'last_name' => 'string|max:50',
            'middle_name' => 'string|max:50',
            'sex' => [
                'string',
                Rule::in(['male', 'female'])
            ],
            'salary' => 'integer|min:1',
            'departments' => 'required|array',
        ]);

        $worker = new Worker();
        $worker->fill($request->all());

        if ($worker->save()) {
            $worker->departments()->sync($request->departments);

            //Обновляем информацию по разделам
            foreach($worker->departments as $department) {
                $department->calculateMaxWorkerSalary();
                $department->calculateWorkerCount();
            }
            return new WorkerResource($worker);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Worker $worker)
    {
        //
        $with = $request->query->get('with', []);

        if (count($with))
            $worker->load($with);

        return new WorkerResource($worker);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worker $worker)
    {
        //
        $this->validate($request,[
            'first_name' => 'string|max:20',
            'last_name' => 'string|max:50',
            'middle_name' => 'string|max:50',
            'sex' => [
                'string',
                Rule::in(['male', 'female'])
            ],
            'salary' => 'integer|min:1',
            'departments' => 'required|array',
        ]);

        $worker->fill($request->all());

        if ($worker->save()) {

            $old_departments = $worker->departments;
            $worker->departments()->sync($request->departments);

            // TODO: Оптимизировать это, чтобы не делать вычисления на повторяющихся отделах.
            foreach($worker->departments as $department) {
                $department->calculateMaxWorkerSalary();
                $department->calculateWorkerCount();
            }

            foreach($old_departments as $department) {
                $department->calculateMaxWorkerSalary();
                $department->calculateWorkerCount();
            }

            return new WorkerResource($worker);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worker  $worker
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worker $worker)
    {
        //
        $temp_departments = $worker->departments;
        $worker->departments()->sync([]);

        foreach($temp_departments as $department) {
            $department->calculateMaxWorkerSalary();
            $department->calculateWorkerCount();
        }

        if ($worker->delete()) {
            return new WorkerResource($worker);
        }
    }
}
