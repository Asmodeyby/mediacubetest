<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Worker
 *
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string|null $middle_name
 * @property string $sex
 * @property float $salary
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereSalary($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereSex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Worker whereUpdatedAt($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Department[] $departments
 */
class Worker extends Model
{
    //
    protected $fillable = ["first_name", "last_name", "middle_name", "sex", "salary"];

    public function departments() {
        return $this->belongsToMany(Department::class, 'department_worker', "worker_id", "department_id");
    }
}
